package common.appium.waiter;

import com.google.common.base.Function;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.htmlelements.element.TextInput;

import java.util.concurrent.TimeUnit;

/**
 * Created by Epifanov on 1/16/2017.
 */
public class CustomWaiter {
    private static final Logger logger = LoggerFactory.getLogger(CustomWaiter.class);

    private WebDriver webDriver;
    private int timeOutInSec;

    public CustomWaiter(WebDriver webDriver, int timeOutInSec) {
        this.webDriver = webDriver;
        this.timeOutInSec = timeOutInSec;
    }

    public WebElement untilWouldBeAvailabilityOfField(TextInput textInput) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver)
                .withTimeout(timeOutInSec, TimeUnit.SECONDS)
                .pollingEvery(100, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        WebElement out = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                textInput.sendKeys("f");
                textInput.clear();
                return textInput.getWrappedElement();
            }
        });
        return out;
    }

    public WebElement untilWouldBeVisibilityOf(WebElement webElement) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver)
                .withTimeout(timeOutInSec, TimeUnit.SECONDS)
                .pollingEvery(100, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class, WebDriverException.class);
        WebElement out = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                webElement.getText();
                return webElement;
            }
        });
        return out;
    }
}
