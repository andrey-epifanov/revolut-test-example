package common.appium;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;

/**
 * Created by Epifanov on 12/12/2016.
 */
public class AndroidDriverLoggingWrapper implements WebDriver, TakesScreenshot, JavascriptExecutor {
    private static Logger logger = LoggerFactory.getLogger(AndroidDriverLoggingWrapper.class);

    private AndroidDriver driver;

    public AndroidDriverLoggingWrapper(AndroidDriver driver) {
        //super(driver.getCapabilities());
        this.driver = driver;
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    @Override
    public Object executeScript(String s, Object... objects) {
        return executeScript(s);
    }

    @Override
    public Object executeAsyncScript(String s, Object... objects) {
        return executeScript(s);
    }

    public Object executeScript(String script) {
        // logging in AndroidDriverFramework
        //logger.info("JS. Execute script = " + script);
        return ((JavascriptExecutor)driver).executeScript(script);
    }

    @Override
    public void get(String s) {
        driver.get(s);
        logger.info("Open url: " + s);
    }

    @Override
    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    @Override
    public String getTitle() {
        return driver.getTitle();
    }

    @Override
    public List<WebElement> findElements(By by) {
//        try {
            return driver.findElements(by);
        //TODO : this is for FireFox - when we can try not do this for any browser
        //TODO : WebDriverException - RunTime , but another exceptions is Throwable - we need it
//        } catch (WebDriverException e) {
//            String message = StringUtils.removeIn(e.getMessage(), "profile=\\S*, ");
//            throw new WebDriverException(message);
//        }
    }

    @Override
    public WebElement findElement(By by) {
//        try {
            return driver.findElement(by);
        //TODO : this is for FireFox - when we can try not do this for any browser
        //TODO : WebDriverException - RunTime , but another exceptions is Throwable - we need it
//        } catch (WebDriverException e) {
//            String message = StringUtils.removeIn(e.getMessage(), "profile=\\S*, ");
//            throw new WebDriverException(message);
//        }
    }

    @Override
    public String getPageSource() {
        return driver.getPageSource();
    }

    @Override
    public void close() {
        driver.close();
        logger.info("Close driver");
    }

    @Override
    public void quit() {
        driver.quit();
        logger.info("Quit");
    }

    @Override
    public Set<String> getWindowHandles() {
        return driver.getWindowHandles();
    }

    @Override
    public String getWindowHandle() {
        return driver.getWindowHandle();
    }

    @Override
    public TargetLocator switchTo() {
        return driver.switchTo();
    }

    @Override
    public Navigation navigate() {
        logger.info("Start navigate");
        return driver.navigate();
    }

    @Override
    public Options manage() {
        return driver.manage();
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return ((TakesScreenshot) driver).getScreenshotAs(outputType);
    }

}
