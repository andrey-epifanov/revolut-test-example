package common.appium.elements;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Epifanov on 12/9/2016.
 */
public class SelectWrapper extends ru.yandex.qatools.htmlelements.element.Select {
    private static Logger logger = LoggerFactory.getLogger(SelectWrapper.class);

    public SelectWrapper(WebElement wrappedElement) {
        super(wrappedElement);
    }

    @Override
    public void selectByVisibleText(String text) {
        logger.info("Select by Text = " + text );
        super.selectByVisibleText(text);
    }

    @Override
    public void selectByIndex(int index) {
        logger.info("Select by Index = " + index + ". Text = " + getOptions().get(index).getText());
        super.selectByIndex(index);
    }

    @Override
    public void selectByValue(String value) {
        logger.info("Select by Value = " + value);
        super.selectByValue(value);
    }

    @Override
    public void click() {
        String nameToLog;
        if (getName() == null)
            nameToLog = getText();
        else
            nameToLog = getName();
        logger.info("Button = " + nameToLog + ". Click.");
        if (!super.isEnabled())
            logger.warn("Button = " + nameToLog + ". Not Enabled!");
        super.click();
    }
}
