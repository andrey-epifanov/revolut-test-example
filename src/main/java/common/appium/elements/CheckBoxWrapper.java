package common.appium.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Epifanov on 2/2/2017.
 */
public class CheckBoxWrapper extends ru.yandex.qatools.htmlelements.element.CheckBox {
    private static Logger logger = LoggerFactory.getLogger(CheckBoxWrapper.class);

    private final static String ATTRIBUT_OF_CLASS_WHEN_SELECTED = "filter-set";

    /**
     * Specifies wrapped {@link WebElement}.
     *
     * @param wrappedElement {@code WebElement} to wrap.
     */
    public CheckBoxWrapper(WebElement wrappedElement) {
        super(wrappedElement);
    }

    /**
     * Selects checkbox if it is not already selected.
     */
    public void select() {
        if (!isSelected()) {
            getWrappedElement().findElement(By.cssSelector("input")).click();
            logger.info("CheckBox = " + getWrappedElement().getText() + ". Operation - Select.");
        } else {
            logger.info("CheckBox = " + getWrappedElement().getText() + ". Selected already.");
        }
    }

    /**
     * Deselects checkbox if it is not already deselected.
     */
    public void deselect() {
        if (isSelected()) {
            getWrappedElement().findElement(By.cssSelector("input")).click();
            logger.info("CheckBox = " + getWrappedElement().getText() + ". Operation - DeSelect.");
        } else {
            logger.info("CheckBox = " + getWrappedElement().getText() + ". DeSelected already.");
        }
    }

    @Override
    public boolean isSelected() {
        String attrClass = getWrappedElement().findElement(By.cssSelector("a span")).getAttribute("class");
        return attrClass.equals(ATTRIBUT_OF_CLASS_WHEN_SELECTED);
    }

    @Override
    public String getText() {
        return getWrappedElement().getText();
    }
}
