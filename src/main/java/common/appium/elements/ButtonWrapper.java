package common.appium.elements;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Epifanov on 12/9/2016.
 */
public class ButtonWrapper extends ru.yandex.qatools.htmlelements.element.Button {
    private static Logger logger = LoggerFactory.getLogger(ButtonWrapper.class);

    public ButtonWrapper(WebElement wrappedElement) {
        super(wrappedElement);
    }

    @Override
    public void click() {
        String nameToLog;
        if (getName() == null)
            nameToLog = getText();
        else
            nameToLog = getName();
        logger.info("Button = " + nameToLog + ". Click.");
        if (!super.isEnabled())
            logger.warn("Button = " + nameToLog + ". Not Enabled!");
        super.click();
    }
}
