package common.appium.elements;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Epifanov on 12/9/2016.
 */
public class LinkWrapper extends ru.yandex.qatools.htmlelements.element.Link {
    private static Logger logger = LoggerFactory.getLogger(LinkWrapper.class);

    public LinkWrapper(WebElement wrappedElement) {
        super(wrappedElement);
    }

    @Override
    public void click() {
        super.click();
        logger.info("Link = " + getName() + ". Click.");
    }
}
