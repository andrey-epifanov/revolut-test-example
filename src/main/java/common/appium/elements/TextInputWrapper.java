package common.appium.elements;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Epifanov on 12/9/2016.
 */
public class TextInputWrapper extends ru.yandex.qatools.htmlelements.element.TextInput {
    private static Logger logger = LoggerFactory.getLogger(TextInputWrapper.class);

    private StringBuilder sb;

    public TextInputWrapper(WebElement wrappedElement) {
        super(wrappedElement);
    }

    @Override
    public void clear() {
        super.clear();
        logger.info("TextInput = " + getName() + ". Clear.");
    }

    @Override
    public void sendKeys(CharSequence... keysToSend) {
        super.sendKeys(keysToSend);
        sb = new StringBuilder(keysToSend.length);
        for (int i = 0; i < keysToSend.length; i++ )
            sb.append(keysToSend[i]);
        logger.info("TextInput = " + getName() + ". Send keys = " + sb.toString());
    }

}
