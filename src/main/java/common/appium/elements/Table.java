package common.appium.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

/**
  * Created by Epifanov on 12/9/2016.
 */
public class Table extends ru.yandex.qatools.htmlelements.element.Table{

    private By headingBy;
    private By rowsBy;
    private By columnsBy;

    /**
     * Specifies {@link org.openqa.selenium.WebElement} representing table tag.
     *
     * @param wrappedElement {@code WebElement} to wrap.
     */
    public Table(WebElement wrappedElement) {
        super(wrappedElement);
        setHeadingBy(By.xpath(".//th"));
        setRowsBy(By.cssSelector("li"));
        setColumnsBy(By.cssSelector("span .cell"));
    }

    /** Set By, which is used in getHeadings().
     * Default By = By.xpath(".//th")
     *
     * @param headingBy
     */
    public void setHeadingBy(By headingBy) {
        this.headingBy = headingBy;
    }

    /** Set By, which is used in getHeadings().
     * Default By = By.xpath(".//td")
     *
     * @param rowsBy
     */
    public void setRowsBy(By rowsBy) {
        this.rowsBy = rowsBy;
    }

    /** Set By, which is used in getHeadings().
     * Default By = By.xpath(".//td")
     *
     * @param columnsBy
     */
    public void setColumnsBy(By columnsBy) {
        this.columnsBy = columnsBy;
    }

    /**
     * Returns a list of table heading elements ({@code <th>}).
     *
     * Multiple rows of heading elements, ({@code <tr>}), are flattened
     * i.e. the second row, ({@code <tr>}), will follow the first, which can be
     * misleading when the table uses {@code colspan} and {@code rowspan}.
     *
     * @return List with table heading elements.
     */
    public List<WebElement> getHeadings() {
        return getWrappedElement().findElements(headingBy);
    }

    /**
     * Returns table cell elements ({@code <td>}), grouped by rows.
     *
     * @return List where each item is a table row.
     */
    //.findElements(By.xpath(".//li"))
    @Override
    public List<List<WebElement>> getRows() {
        return getWrappedElement()
                .findElements(rowsBy)
                .stream()
                //.map(rowElement -> rowElement.findElements(By.xpath(".//span")))
                .map(rowElement -> rowElement.findElements(columnsBy))
                .filter(row -> row.size() > 0) // ignore rows with no <td> tags
                .collect(toList());
    }

    /**
     * Returns table cell elements ({@code <td>}), of a particular column.
     *
     * @param index the 1-based index of the desired column
     * @return List where each item is a cell of a particular column.
     */
    public List<WebElement> getColumnByIndex(int index) {
        return getWrappedElement().findElements(
                By.cssSelector(String.format("tr > td:nth-of-type(%d)", index)));
    }

    /**
     * Returns table cell element ({@code <td>}), at i-th row and j-th column.
     *
     * @param i Row number
     * @param j Column number
     * @return Cell element at i-th row and j-th column.
     */
    public WebElement getCellAt(int i, int j) {
        return getRows().get(i).get(j);
    }

    /**
     * Same as {@link #getRowsMappedToHeadings()} but retrieves text from row elements ({@code <td>}).
     */
    public List<Map<String, String>> getRowsAsStringMappedToHeadings() {
        return getRowsAsStringMappedToHeadings(getHeadingsAsString());
    }

}