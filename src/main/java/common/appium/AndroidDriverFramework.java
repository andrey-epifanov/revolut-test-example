package common.appium;

import common.appium.waiter.CustomWaiter;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import lombok.Getter;
import lombok.Setter;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.element.TextInput;


/**
 * @author Andrey Epifanov aepifanov@luxoft.ru
 *         Date: 07.12.16
 *
 * Description:
 */
public class AndroidDriverFramework {
    private static Logger logger = LoggerFactory.getLogger(AndroidDriverFramework.class);

    public final static int TIMEOUT_IN_SEC = 10;

    private AndroidDriver driver;
    @Getter
    private WebDriverWait waiter;
    private CustomWaiter customWaiter;
    @Getter
    private Actions action;

    @Setter
    @Getter
    private boolean screenshotInSF;
    @Setter

	public boolean shiftDown = false;
	public boolean ctrlDown = false;

    public AndroidDriverFramework(AndroidDriver driver) {
        this.driver = driver;
        this.waiter = new WebDriverWait(driver, TIMEOUT_IN_SEC);
        this.customWaiter = new CustomWaiter(driver, TIMEOUT_IN_SEC);
//        if (driver instanceof AndroidDriverLoggingWrapper)
//            this.action = new Actions(((AndroidDriverLoggingWrapper)driver).getWrappedDriver());
//        else
//            logger.error("Action is not initialized.");
    }

    public AppiumDriver getInstance() {
        return driver;
    }

    /** now NOT WORK !
     */
    public void openDevTools() {
        action.sendKeys(Keys.F12).perform();
    }

    public void scrollingPageDown() {
        action.sendKeys(Keys.PAGE_DOWN).perform();
    }

    public void scrollingPageUp() {
        action.sendKeys(Keys.PAGE_UP).perform();
    }

    /** usual xOffset = 5 pixel
     *
     * @param webElement
     * @param xOffset
     */
    public void clickRigthAfter(WebElement webElement, int xOffset) {
        action.moveToElement(webElement, webElement.getSize().getWidth() + xOffset, 0).click().build().perform();
    }


    public boolean waitUntil(WebElement webElement, String textInWebElement) {
        return waiter.until(ExpectedConditions.textToBePresentInElement(webElement, textInWebElement));
    }

    public boolean waitUntilNot(WebElement webElement, String textInWebElement) {
        return waiter.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(webElement, textInWebElement)));
    }

    public WebElement waitAvailabilityOfTextInput(TextInput webElement) {
        if (! (webElement instanceof TextInput) )
            throw new RuntimeException("WebElement is not TextInput. WebElement webElement =  " + webElement);
        return waiter.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public WebElement waitVisibilityOf(WebElement webElement) {
        if (webElement instanceof TextInput)
            throw new RuntimeException("WebElement is not TextInput.");
        return waiter.until(ExpectedConditions.visibilityOf(webElement));
    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    @Step("JavaScript")
    public Object executeJS(String script) {
        logger.info("Execute JavaScript = " + script);
        return ((JavascriptExecutor) driver).executeScript(script);
    }

    public byte[] makeScreenShot() {
        return makeScreenShot("");
    }

    @Attachment
    public byte[] makeScreenShot(String suffixName) {
        byte[] screenBytes = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        return screenBytes;
    }

    public void quit() {
        try {
            driver.quit();
        } catch (WebDriverException wde) {
            throw wde;
        }
    }

    /** Cause: FF + Yandex HtmlElements can not work with usual waiter.
     *
     * @return
     */
    //@Deprecated
    public WebDriverWait getWaiter() {
        return waiter;
    }
}
