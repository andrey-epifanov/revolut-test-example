package common.generalhelpers;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created with IntelliJ IDEA.
 * User: ppolishchuk
 * Date: 24.06.13
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */
public class RandomDataGenerator {
    public static String getRandomString(Integer length){
        return getRandomString(length, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz");
    }

    public static String getRandomSpecialChar() {
        return getRandomString(1, "!@#$$%%^&*()_-+=|}{\"'/\\:№[]?*/ ~`");
    }

    public static String getRandomString(Integer length, String from){
        Random ran = new Random();
        String characters = from;
        char[] text = new char[length];
        for (int i = 0; i < length; i++)
        {
            text[i] = characters.charAt(ran.nextInt(characters.length()));
        }
        return new String(text);
    }

    public static String getRandomNumericString(Integer length){
        Random ran = new Random();
        String characters = "01234567890123456789012345678901234567890123456789";
        char[] text = new char[length];
        for (int i = 0; i < length; i++)
        {
            text[i] = characters.charAt(ran.nextInt(characters.length()));
        }
        return new String(text);
    }

    private static String getRandomNumberAsString(Integer length){
        if(length.equals(0)){
            return "0";
        }
        Random ran = new Random();
        String characters = "01234567890123456789012345678901234567890123456789";
        Integer realLength = 1 + ran.nextInt(length);
        char[] text = new char[realLength];
        text[0] = ((Integer)(1 + ran.nextInt(9))).toString().charAt(0);
        for (int i = 1; i < realLength; i++)
        {
            text[i] = characters.charAt(ran.nextInt(characters.length()));
        }
        text[realLength-1] = ((Integer)(1 + ran.nextInt(9))).toString().charAt(0);
        return new String(text);
    }

    public static Integer getRandomInteger(Integer length){
        if(length.equals(0)){
            return 0;
        } else if(length > 9){
            return new Random().nextInt(Integer.MAX_VALUE);
        }
        return new Random().nextInt((int) Math.pow(10, length));
    }

    public static Integer getRandomInteger(Integer minValue, int maxValue){
        Random random = new Random();
        int randomNumber = random.nextInt(maxValue - minValue) + minValue;
        return randomNumber;
    }

    public static Long getRandomLong(Integer length){
        if(length.equals(0)){
            return 0L;
        } else if(length > 19){
            return ThreadLocalRandom.current().nextLong(Long.MAX_VALUE);
        }
        Long randomNumber = ThreadLocalRandom.current().nextLong(new Double(Math.pow(10, length)).longValue());
        return randomNumber;
    }

    public static Long getRandomLong(Long minValue, Long maxValue){
        Long randomNumber = ThreadLocalRandom.current().nextLong(maxValue - minValue) + minValue;
        return randomNumber;
    }

    public static String getRandomLongHexOneDigit(){
        return getRandomLongHex("0", "F");
    }

    public static String getRandomLongHex(String min, String max){
        int input = getRandomInteger( new BigInteger(min, 16).intValue(), new BigInteger(max, 16).intValue() );
        return Long.toHexString(new Integer(input).longValue());
    }

    public static BigDecimal getRandomBigDecimal(Integer length, Integer getMaxFraction){
        BigDecimal integer = new BigDecimal(getRandomNumberAsString(length));
        BigDecimal fraction = new BigDecimal("0." + getRandomNumberAsString(getMaxFraction));
        return integer.add(fraction);
    }

    public static String getRandomDoubleAsString(Integer length, Integer getMaxFraction) {
        return getRandomBigDecimal(length - getMaxFraction, getMaxFraction).toString();
    }

    public static String getRandomDoubleAsString(Double from, Double to, Integer getMaxFraction){
        BigDecimal difference = BigDecimal.valueOf(to).subtract(BigDecimal.valueOf(from));
        BigDecimal middle = BigDecimal.valueOf(to).add(BigDecimal.valueOf(from)).divide(new BigDecimal("2"));
        if(difference.compareTo(new BigDecimal("0")) == 0){
            return BigDecimal.valueOf(to).stripTrailingZeros().toPlainString();
        } else if(difference.compareTo(new BigDecimal("0")) == -1 ){
            throw new IllegalArgumentException();
        } else {
            BigDecimal range = BigDecimal.valueOf(to).subtract(middle);
            return middle.add(getRandomBigDecimal(((int)Math.log10(range.doubleValue())), getMaxFraction)).stripTrailingZeros().toPlainString();
        }
    }



    public static DateTime getRandomDate(){
        return getRandomDate(1970);
    }

    public static DateTime getRandomDate(Integer minYear){
        GregorianCalendar gc = new GregorianCalendar();
        int year = minYear + new Random().nextInt(50);
        gc.set(GregorianCalendar.YEAR, year);
        int dayOfYear = new Random().nextInt(gc.getActualMaximum(GregorianCalendar.DAY_OF_YEAR));
        gc.set(GregorianCalendar.DAY_OF_YEAR, dayOfYear);
        gc.set(GregorianCalendar.HOUR_OF_DAY, new Random().nextInt(24));
        gc.set(GregorianCalendar.MINUTE, new Random().nextInt(60));
        gc.set(GregorianCalendar.SECOND, new Random().nextInt(60));
        return new DateTime(gc);
    }

    public static <T extends Enum> Enum getRandomEnumValue(Class<T> enumData){
        List<T> values =
                Collections.unmodifiableList(Arrays.asList(enumData.getEnumConstants()));
        Integer selected = new Random().nextInt(values.size());
        return values.get(selected);


    }

    public static <T> T getRandomValueFromList(List<T> values) {
        Integer selected = new Random().nextInt(values.size());
        return values.get(selected);
    }

    public static boolean getRandomBoolean(){
        Integer selected = new Random().nextInt(2);
        if(selected.equals(0)){
            return true;
        } else {
            return false;
        }
    }
}
