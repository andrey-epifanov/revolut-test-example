package common.selenium.baseanalyser;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

/**
 * The class implements Base-annotation logic.
 *
 * Created by Yury Kosterin && Andrey Epifanov on 06.06.2017.
 */
public class BasePageAnalyser {

	private static Logger logger = LoggerFactory.getLogger(BasePageAnalyser.class);

	/**
	 * Gets all page object fields and checks for Base annotation presence. In
	 * case of mandatory field checks the element presence on the page.
	 *
	 * @param command
	 *            sets the way of checking web element
	 * @param page
	 *            the tested page object
	 * @return {@code true} if all mandatory web elements are present on the
	 *         page, {@code false} otherwise
	 */
	public static boolean check(ICommandCheck command, Object page) {
		final Field[] fields = page.getClass().getFields();
		boolean result = false;
		for (final Field field : fields) {
			if (field.isAnnotationPresent(Base.class)) {
				final Base base = field.getAnnotation(Base.class);
				final String type = base.checkType();
				if ("mandatory".equals(type)) {
					result = checkField(command, field, page);
				} else
					result = true;
			}
		}
		return result;
	}

	private static boolean checkField(ICommandCheck command, Field field, Object page) {
		field.setAccessible(true);
		boolean result = false;
		try {
			final Object element = field.get(page);
			if (element instanceof WebElement) {
				command.setElement((WebElement) element);
				command.run();
				result = command.getResult();
			} else
				logger.debug(String.format("The field %s is not WebElement", field.toString()));
		} catch (IllegalAccessException e) {
			logger.debug(String.format("Exception on field %s", field));
			e.printStackTrace();
		}
		return result;
	}
}
