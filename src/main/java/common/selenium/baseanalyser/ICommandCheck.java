package common.selenium.baseanalyser;

import org.openqa.selenium.WebElement;

/**
 * Created by Yury Kosterin && Andrey Epifanov on 09.06.2017.
 */
public interface ICommandCheck extends Runnable {

    WebElement getResultWebElement();

    boolean getResult();

    void setElement(WebElement element);
}
