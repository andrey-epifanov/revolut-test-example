package common.selenium.baseanalyser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Yury Kosterin && Andrey Epifanov on 09.06.2017.
 */
public class CommandCheckIsEnabled extends BaseCommandCheck {

	private static Logger logger = LoggerFactory.getLogger(CommandCheckIsEnabled.class);

	@Override
	public void run() {
		if (element.isDisplayed())
			result = true;
		else {
			logger.debug(String.format("element %s is not displayed", element.toString()));
			result = element.isEnabled();
		}
	}

	@Override
	public boolean getResult() {
		if (!super.getResult()) {
			logger.error(String.format("element %s is not enabled", element.toString()));
			return false;
		}
		return true;
	}
}
