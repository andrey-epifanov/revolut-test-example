package common.selenium.baseanalyser;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Yury Kosterin && Andrey Epifanov on 06.06.2017.
 */
@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Base {
	String checkType() default "mandatory";
}
