package common.selenium.baseanalyser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Yury Kosterin && Andrey Epifanov on 09.06.2017.
 */
public abstract class BaseCommandCheck implements ICommandCheck {

    protected WebDriver driver;
    protected String browserType;

    protected WebElement element;
    protected Boolean result = false;

    protected BaseCommandCheck() {
    }

    protected BaseCommandCheck(WebDriver driver, String browserType) {
        this.browserType = browserType;
        this.driver = driver;
     }

    public void setElement(WebElement element) {
        this.element = element;
        result = false;
    }

    @Override
    public WebElement getResultWebElement() {
        return element;
    }

    @Override
    public boolean getResult() {
        return result;
    }
}
