package common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by aepifanov on 18.11.2016.
 */
public class SleepUtils {
    private static Logger logger = LoggerFactory.getLogger(SleepUtils.class);

    public final static int SLEEP__500_MILLIS = 500;
    public final static int SLEEP_1000_MILLIS = 1000;

    public static void sleep_500_msec() {
        sleep(SLEEP__500_MILLIS);
    }

    public static void sleep_01_sec() {
        sleep(SLEEP_1000_MILLIS);
    }

    public static void sleep_02_sec() {
        sleep(SLEEP_1000_MILLIS);
        sleep(SLEEP_1000_MILLIS);
    }

    public static void sleep(int ms) {
        try {
            logger.info("Sleep for " + ms + " ms");
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
