package common.utils;

import org.slf4j.Logger;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by Epifanov on 12/20/2016.
 */
public class LogUtils {

    @Step("Info")
    public static void info(Logger logger, String str) {
        logger.info(str);
    }

    @Step("Warn")
    public static void warn(Logger logger, String str) {
        logger.warn(str);
    }

    @Step("Error")
    public static void error(Logger logger, String str) {
        logger.error(str);
    }
}
