package revolut.data;

/**
 * Created by aepifanov on 12.08.2017.
 */
public enum CardType {
    VISA,
    VISA_DEBIT,
    VISA_ELECTRON,
    MASTER_CARD,
    MASTER_CARD_DEBIT,
    AMERICAN_EXPRESS,
    DISCOVER,
    JCB
}
