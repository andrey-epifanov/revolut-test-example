package revolut.data;

/**
 * Created by aepifanov on 12.08.2017.
 */
public enum Status {
    AVAILABLE_FOR_ADD,
    ADDED
}
