package revolut.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by aepifanov on 13.08.2017.
 */
@Getter
@Setter
@AllArgsConstructor
public class CurrencyVolume {
    private String type;
    private String amount;
}
