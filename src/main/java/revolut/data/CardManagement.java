package revolut.data;

import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by aepifanov on 12.08.2017.
 */
@Getter
public class CardManagement {
    private Logger logger = LoggerFactory.getLogger(CardManagement.class);

    private List<Card> availableForAdd = Arrays.asList(Card.values());
    private List<Card> added = new ArrayList<>();

    public CardManagement(){}

    public void addAddedCard(Card card) {
        if(!added.contains(card))
            added.add(card);
        if(availableForAdd.contains(card))
            availableForAdd.remove(card);
    }
}
