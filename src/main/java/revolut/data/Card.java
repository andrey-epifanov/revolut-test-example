package revolut.data;


import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

import static revolut.data.P3DSecureEnabled.NO;
import static revolut.data.P3DSecureEnabled.YES;

/**
 * Created by aepifanov on 12.08.2017.
 */
@Getter
@ToString
public enum Card {
    C_2345(CardType.VISA, "4530910000012345", YES),
    C_0321(CardType.VISA, "4510150000000321", NO),
    C_3670(CardType.VISA, "4107857757053670", YES),

    C_3775(CardType.VISA_DEBIT, "4206720389883775", YES),
    C_0008(CardType.VISA_ELECTRON, "4917480000000008", NO),
    C_7107(CardType.VISA_ELECTRON, "4917484589897107", NO),

    C_4415(CardType.MASTER_CARD, "5191330000004415", NO),
    C_8763(CardType.MASTER_CARD, "5457490000008763", NO),
    C_7720(CardType.MASTER_CARD, "5186750368967720", YES),

    C_0162(CardType.MASTER_CARD_DEBIT, "6759950000000162", NO),
    C_1115(CardType.MASTER_CARD_DEBIT, "5036150000001115", NO),
    C_2200(CardType.MASTER_CARD_DEBIT, "5573560100022200", YES),

    C_1002(CardType.AMERICAN_EXPRESS, "375529360131002", NO),
    C_9017(CardType.AMERICAN_EXPRESS, "370123456789017", NO),

    C_0123(CardType.DISCOVER, "6011234567890123", NO),
    C_0009(CardType.JCB, "3569990000000009", NO);

    private CardType cardType;
    private String number;
    private P3DSecureEnabled p3dSecure;

    Card(CardType cardType, String number, P3DSecureEnabled p3dSecure){
        this.cardType = cardType;
        this.number = number;
        this.p3dSecure = p3dSecure;
    }

    public String getLast4digits() {
        return extractLast4digits(number);
    }

    //TODO: refactor - use HashMap
    public static Card searchByLast4digit(String last4digit) {
        for (Card card : values()) {
            if (card.getNumber().endsWith(last4digit))
                return card;
        }
        return null;
    }

    public static String extractLast4digits(String number){
        return number.substring(number.length() - 4, number.length());
    }

    public static List<Card> getCardsBy(P3DSecureEnabled p3dSecure){
        List<Card> out = new ArrayList<>();
        for(Card card:values()){
            if (card.getP3dSecure().equals(p3dSecure))
                out.add(card);
        }
        return out;
    }
}
