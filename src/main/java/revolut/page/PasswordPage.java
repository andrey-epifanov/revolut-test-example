package revolut.page;

import common.appium.AndroidDriverFramework;
import common.selenium.baseanalyser.Base;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by aepifanov on 11.08.2017.
 */
public class PasswordPage extends AbstractPage {
    protected PasswordPage(AndroidDriverFramework webDriver) {
        super(webDriver);
    }

    @Base
    @FindBy(id = "com.revolut.revolut.test:id/digit1")
    public WebElement digit_1;
}
