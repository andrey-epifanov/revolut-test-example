package revolut.page.action;

import common.appium.AndroidDriverFramework;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import revolut.page.StartPhonePage;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by aepifanov on 11.08.2017.
 */
public class StartPhoneAction extends StartPhonePage {
    private Logger logger = LoggerFactory.getLogger(StartPhoneAction.class);

    public StartPhoneAction(AndroidDriverFramework webDriver) {
        super(webDriver);
    }

    @Step("StartPhoneAction. Execute.")
    public void executeWith(String phone){
        logger.info("StartPhoneAction. Start");

        driver.makeScreenShot("1");
        logger.info("Phone = " + phone);
        edit_phone_number.clear();
        edit_phone_number.sendKeys(phone);

        driver.makeScreenShot("2");
        logger.info("StartPhoneAction. btn_sgnup - click.");
        btn_sgnup.click();

        driver.makeScreenShot("3");
        logger.info("StartPhoneAction. Done.");
    }
}
