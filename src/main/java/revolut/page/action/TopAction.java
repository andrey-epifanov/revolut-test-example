package revolut.page.action;

import common.appium.AndroidDriverFramework;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import revolut.page.TopPage;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by aepifanov on 11.08.2017.
 */
public class TopAction extends TopPage {
    private Logger logger = LoggerFactory.getLogger(TopAction.class);

    public TopAction(AndroidDriverFramework webDriver) {
        super(webDriver);
    }

    @Step("TopAction. Execute.")
    public void execute(){
        logger.info("TopAction. Start");
        btn_topUp.click();
        logger.info("TopAction. Done.");
    }
}
