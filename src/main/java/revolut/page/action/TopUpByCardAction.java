package revolut.page.action;

import common.appium.AndroidDriverFramework;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import revolut.data.Card;
import revolut.data.CurrencyVolume;
import revolut.page.TopUpByCard;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by aepifanov on 11.08.2017.
 */
public class TopUpByCardAction extends TopUpByCard {
    private Logger logger = LoggerFactory.getLogger(TopUpByCardAction.class);

    public TopUpByCardAction(AndroidDriverFramework webDriver) {
        super(webDriver);
    }

    @Step("TopUpByCardAction. Back.")
    public void back(){
        logger.info("TopUpByCardAction. Back");
        btn_backButton.click();
    }

    @Step("TopUpByCardAction. Card - edit address.")
    public void cardEditAddress(){
        logger.info("TopUpByCardAction. Card - edit.");
        btn_cardEditButton.click();

        logger.info("TopUpByCardAction. Card - edit address.");
        getEditAddress().click();
    }

    @Step("TopUpByCardAction. Card - delete.")
    public void cardDelete(){
        logger.info("TopUpByCardAction. Card - edit.");
        btn_cardEditButton.click();
        driver.makeScreenShot("1");

        logger.info("TopUpByCardAction. Card - delete.");
        getDelete().click();
        driver.makeScreenShot("2");

        logger.info("TopUpByCardAction. Card - delete. Yes(Confirm).");
        btn_yes.click();
        driver.makeScreenShot("3");
    }

    /** before it - card page should be clear .
     *
     * @param card
     */
    @Step("TopUpByCardAction. Card - add.")
    public void cardAdd(Card card){
        logger.info("TopUpByCardAction. Card - add.");
        driver.makeScreenShot("1");
        text_cardNumber.clear();
        text_cardNumber.sendKeys(card.getNumber());

        text_cardExpirationDate.clear();
        text_cardExpirationDate.sendKeys("1121"); // 11 - month, 21 - year

        text_cardCvv.clear();
        text_cardCvv.sendKeys("123");

        driver.makeScreenShot("2");
        logger.info("TopUpByCardAction. Card - add. Done.");
        btn_main.click();

        driver.makeScreenShot("3");
    }

    @Step("TopUpByCardAction. Card - get current Card.")
    public Card getCurrentCard() {
        logger.info("TopUpByCardAction. Card - get current Card.");
        String last4digit = text_cardNumber.getText().replaceAll("\\*", "").trim();
        if (last4digit.length() == 0) {
            logger.info("TopUpByCardAction. Current card text = nothing");
            return null;
        }
        logger.info("TopUpByCardAction. Current card text = " + text_cardNumber.getText() + "; Extracted 4 last digits = " + last4digit);
        Card card = Card.searchByLast4digit(last4digit);
        logger.info("TopUpByCardAction. Current in list of known cards = " + card);
       return card;
    }

    @Step("TopUpByCardAction. Move to add new card.")
    public void moveToAddNewCard() {
        logger.info("TopUpByCardAction. Move to add new card.");

        //TODO: not work - TouchAction
//        text_cardNumber.click();
//        MobileElement dragMe = (MobileElement) new WebDriverWait(driver, 30)
//                .until(ExpectedConditions
//                        .elementToBeClickable(MobileBy.AccessibilityId("com.revolut.revolut.test:id/card_number_text")));
        Dimension dimension = driver.getInstance().manage().window().getSize();

        //TODO: not work - TouchAction
        try {
            driver.getInstance().performTouchAction(new TouchAction(driver.getInstance()))
                    .tap(dimension.getWidth() - 50, btn_cardEditButton.getLocation().getY())
                    .moveTo(50, btn_cardEditButton.getLocation().getY())
                    .release().perform();
        }catch (WebDriverException wde){
            logger.info("exception = " + wde.getMessage());
        }
//
//        Actions actions = new Actions(driver);
//        actions.clickAndHold(btn_cardEditButton)
//                .moveByOffset(btn_cardEditButton.getLocation().getX(), 10).build().perform();

//        ob.moveToElement(icon);
//        ob.click(icon);
//        Action action  = ob.build();
//        action.perform();

//        new TouchAction(driver).press( text_cardNumber.getLocation().getX(), dimension.getWidth() - 10 ).waitAction(3000)
//                .moveTo(text_cardNumber.getLocation().getX(), 10 ).release().perform();

        //String expected = driver.findElementByAccessibilityId("success").getText();
        //assertEquals(expected, "Circle dropped");
    }

    @Step("TopUpByCardAction. Execute.")
    public void executeWith(CurrencyVolume currencyVolume, Point digit1point) {
        logger.info("TopUpByCardAction. Execute.");

        logger.info("Currency Volume = " + currencyVolume);
        driver.makeScreenShot("1");

        chooseCurrency(currencyVolume.getType());

        text_cardCvv.clear();
        text_cardCvv.click();
        //text_cardCvv.sendKeys("1"); // TODO: now not important , verify it in future

        //TODO: not work
//        text_cardCvv.click();
//        driver.getInstance().getKeyboard().pressKey("1");
//        driver.getInstance().getKeyboard().pressKey("2");
//        driver.getInstance().getKeyboard().pressKey("3");

//      //TODO: not work
//        text_cardNumber.click();
//        text_cardCvv.sendKeys("2");
//        text_cardNumber.click();
//        text_cardNumber.click();
//        text_cardCvv.sendKeys("3");
//        text_cardNumber.click();
//        text_cardNumber.click();

        //TODO: not work
//        ((MobileElement)text_cardCvv).setValue("123");
//        ((MobileElement)text_cardCvv).setValue(text_cardCvv.getText() + "23");

        //TODO: attempt to use real digit keyboard on device
        for (int i=0; i < 3 ; i++ ) {
            // digit 1
            try {
                //TODO: point is not equal
//                driver.getInstance().performTouchAction(new TouchAction(driver.getInstance()))
//                        .press(digit1point.getX(), digit1point.getY())
//                        .release().perform();

                driver.getInstance().performTouchAction(new TouchAction(driver.getInstance()))
                        .press(200, 1200)
                        .release().perform();
            } catch (WebDriverException wde) {
            }
        }

        input_hint_amount.sendKeys(currencyVolume.getAmount());
        driver.makeScreenShot("2");

        // TODO: work!!!
        ((MobileElement)text_cardCvv).setValue("123");

        btn_main.click();

        try{ // thropugth if
            if(btn_yes.isDisplayed()) {
                logger.info("Btn yes,continue - click");
                driver.makeScreenShot("3");
                btn_yes.click();
            }
        }catch(NotFoundException nfe){
            logger.info("Btn yes,continue - is not shown");
            logger.info("exception = " + nfe.getMessage());
        }

        driver.makeScreenShot("4");
        logger.info("TopUpByCardAction. Execute. Done");
    }

    @Step("Choose currency type.")
    private void chooseCurrency(String currencyType) {
        logger.info("TopUpByCardAction. Choose Currency.");
        // todo: choose currency
        driver.makeScreenShot("1");
//        if(btn_currency.getText().equals(currencyType))
        btn_currency.click();
        for(WebElement avalableCurrencyType : btn_cardWrappers){
            if (avalableCurrencyType.getText().equals(currencyType)) {
                avalableCurrencyType.click();
                driver.makeScreenShot("2");
                return;
            }
        }
    }

    @Step("TopUpByCardAction. Verify.")
    public boolean verifyCard(Card card) {
        logger.info("TopUpByCardAction. Verify Card.");

        driver.makeScreenShot("1");
        String actual = Card.extractLast4digits(text_cardNumber.getText());

        logger.info("Actual = " + actual + " ; Expected = " + card.getLast4digits());
        return actual.equals(card.getLast4digits());
    }
}
