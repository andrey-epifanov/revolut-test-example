package revolut.page.action;

import common.appium.AndroidDriverFramework;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import revolut.page.AllowContactAccessPage;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by aepifanov on 11.08.2017.
 */
public class AllowContactAccessAction extends AllowContactAccessPage {
    private Logger logger = LoggerFactory.getLogger(AllowContactAccessAction.class);

    public AllowContactAccessAction(AndroidDriverFramework webDriver) {
        super(webDriver);
    }

    @Step("PasswordAction. Execute.")
    public void execute() {
        logger.info("PasswordAction. Start");

        logger.info("btn_allow_contact_access. Click.");
        btn_allow_contact_access.click();

        logger.info("btn_allow. Click.");
        if(btn_allow.isDisplayed())
            btn_allow.click();
        else
            logger.info("PasswordAction. Btn Allow is not exist.");

        logger.info("PasswordAction. Done");
    }
}
