package revolut.page.action;

import common.appium.AndroidDriverFramework;
import org.openqa.selenium.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import revolut.page.PasswordPage;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by aepifanov on 11.08.2017.
 */
public class PasswordAction extends PasswordPage {
    private Logger logger = LoggerFactory.getLogger(PasswordAction.class);

    private Point digit1point;

    public PasswordAction(AndroidDriverFramework webDriver) {
        super(webDriver);
    }

    @Step("PasswordAction. Execute.")
    public void execute(){
        logger.info("PasswordAction. Start");
        //digit1point = digit_1.getLocation();
        digit_1.click();
        digit_1.click();
        digit_1.click();
        digit_1.click();
        logger.info("PasswordAction. Done.");
    }

//    public Point getLocationDigit1() {
//        return digit1point;
//    }
}
