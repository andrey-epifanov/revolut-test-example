package revolut.page;

import common.appium.AndroidDriverFramework;
import common.selenium.baseanalyser.Base;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by aepifanov on 11.08.2017.
 */
public class TopPage extends AbstractPage {
    protected TopPage(AndroidDriverFramework webDriver) {
        super(webDriver);
    }

    @FindBy(className = "android.widget.TextView")
    public List<WebElement> text_views;

    @FindBy(id = "com.revolut.revolut.test:id/amount_int")
    public WebElement text_amount_int;

    public WebElement getCurrancyValueInt() { // main part - int
        return text_views.get(4); // TODO: in future - check
    }

    @Base
    @FindBy(id = "com.revolut.revolut.test:id/ccy_description")
    public WebElement text_ccy_description;

    @FindBy(id = "com.revolut.revolut.test:id/left_button")
    public WebElement btn_leftBtn;

    @FindBy(id = "com.revolut.revolut.test:id/right_button")
    public WebElement btn_rigthBtn;

    @FindBy(id = "com.revolut.revolut.test:id/button_top_up")
    public WebElement btn_topUp;

    @Base
    @FindBy(id = "com.revolut.revolut.test:id/button_transfer")
    public WebElement btn_transfer;
}
