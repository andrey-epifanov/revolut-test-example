package revolut.page;

import common.appium.AndroidDriverFramework;
import common.selenium.baseanalyser.Base;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by aepifanov on 12.08.2017.
 */
public class TopUpByCard extends AbstractPage{
    protected TopUpByCard(AndroidDriverFramework webDriver) {
        super(webDriver);
    }

    @Base
    @FindBy(id = "com.revolut.revolut.test:id/back_button")
    public WebElement btn_backButton;

    @Base
    @FindBy(id = "com.revolut.revolut.test:id/card_edit_button")
    public WebElement btn_cardEditButton;

    @Base
    @FindBy(id = "com.revolut.revolut.test:id/card_number_text")
    public WebElement text_cardNumber;

    @FindBy(id = "com.revolut.revolut.test:id/card_expiration_date")
    public WebElement text_cardExpirationDate;

    @FindBy(id = "com.revolut.revolut.test:id/card_cvc")
    public WebElement text_cardCvv;

    @FindBy(id = "com.revolut.revolut.test:id/button_currency")
    public WebElement btn_currency; // TODO: or select

    @FindBy(id = "com.revolut.revolut.test:id/hint_amount")
    public WebElement input_hint_amount;

    @Base
    @FindBy(id = "com.revolut.revolut.test:id/next_button")
    public WebElement btn_main; // TODO: names - TOP_UP , ADD CARD

    @FindBy(id = "com.revolut.revolut.test:id/card_wrapper")
    public List<WebElement> btn_cardWrappers;

    protected WebElement getEditAddress(){
        return btn_cardWrappers.get(0);
    }

    protected WebElement getDelete(){
        return btn_cardWrappers.get(1);
    }

    @FindBy(id = "com.revolut.revolut.test:id/bt_yes")
    public WebElement btn_yes;

    @FindBy(id = "com.revolut.revolut.test:id/bt_no")
    public WebElement btn_cancel; //Todo : names - cancel, no

    @FindBy(id = "com.revolut.revolut.test:id/loading_container")
    public WebElement text_loading_container; // icon for show - loading process

}
