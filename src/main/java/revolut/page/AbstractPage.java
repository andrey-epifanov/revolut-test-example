package revolut.page;

import common.appium.AndroidDriverFramework;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by aepifanov on 11.08.2017.
 */
public abstract class AbstractPage {
    protected AndroidDriverFramework driver;

    AbstractPage(AndroidDriverFramework driver){
        this.driver = driver;
        //TODO: not work now - need rewrite HtmlElementLocatorFactory
        //PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(webDriverFramework.getInstance())), this);
        PageFactory.initElements(new AppiumFieldDecorator(driver.getInstance(), 5, TimeUnit.SECONDS), this);
    }
}
