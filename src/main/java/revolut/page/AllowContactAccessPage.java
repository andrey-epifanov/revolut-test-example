package revolut.page;

import common.appium.AndroidDriverFramework;
import common.selenium.baseanalyser.Base;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by aepifanov on 11.08.2017.
 */
public class AllowContactAccessPage extends AbstractPage {
    protected AllowContactAccessPage(AndroidDriverFramework webDriver) {
        super(webDriver);
    }

    @Base
    @FindBy(id = "com.revolut.revolut.test:id/uic_header_next")
    public WebElement text_header;

    @Base
    @FindBy(id = "com.revolut.revolut.test:id/linearLayout") // Enable instatnt money transfer
    public WebElement text_question;

    @Base
    @FindBy(id = "com.revolut.revolut.test:id/action_button")
    public WebElement btn_allow_contact_access;

    @FindBy(id = "com.android.packageinstaller:id/permission_allow_button")
    public WebElement btn_allow;
}
