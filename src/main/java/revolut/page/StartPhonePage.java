package revolut.page;

import common.appium.AndroidDriverFramework;
import common.selenium.baseanalyser.Base;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by aepifanov on 11.08.2017.
 */
public class StartPhonePage extends AbstractPage {
    protected StartPhonePage(AndroidDriverFramework webDriver) {
        super(webDriver);
    }

    @Base
    @FindBy(id = "com.revolut.revolut.test:id/uic_edit_phone_number")
    public WebElement edit_phone_number;

    @FindBy(id = "com.revolut.revolut.test:id/signup_next")
    public WebElement btn_sgnup;
}
