package revolut.global;

import common.appium.AndroidDriverFramework;
import common.appium.AndroidDriverLoggingWrapper;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServerHasNotBeenStartedLocallyException;
import org.openqa.selenium.remote.DesiredCapabilities;
import revolut.page.action.*;

import java.io.File;

/**
 * Created by aepifanov on 08.08.2017.
 */
abstract class AbstractApp {

    protected AndroidDriverFramework driver;
    private AndroidDriver appiumDriver;
    private AppiumDriverLocalService service;

    protected PasswordAction passwordAction;
    protected AllowContactAccessAction allowContactAccessAction;
    protected TopAction topAction;
    protected TopUpByCardAction topUpByCardAction;
    protected StartPhoneAction startPhoneAction;

    public void setUp() throws Exception {
        service = AppiumDriverLocalService.buildDefaultService();
        service.start();

        if (service == null || !service.isRunning()) {
            throw new AppiumServerHasNotBeenStartedLocallyException(
                    "An appium server node is not started!");
        }
        File classpathRoot = new File(System.getProperty("user.dir"));
        File appDir = new File(classpathRoot, "./app");
        File app = new File(appDir.getCanonicalPath(), "Revolut_qa_4.3.0.237.apk");
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName","Android Emulator");
        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability("appPackage", "com.revolut.revolut.test");
        capabilities.setCapability("appActivity", "com.revolut.ui.login.pin.LoginActivity");

        driver = new AndroidDriverFramework(
                //new AndroidDriverLoggingWrapper(
                        new AndroidDriver<>(service.getUrl(), capabilities)
                //)
        );

        //driver.performTouchAction(new TouchAction(driver).tap(100,100)).;

        initPages();
    }

    private void initPages(){
        passwordAction = new PasswordAction(driver);
        allowContactAccessAction = new AllowContactAccessAction(driver);
        topAction = new TopAction(driver);
        topUpByCardAction = new TopUpByCardAction(driver);
        startPhoneAction = new StartPhoneAction(driver);
    };

    public void tearDown() throws Exception {
        if (driver != null) {
            driver.quit();
        }
        if (service != null) {
            service.stop();
        }
    }

}
