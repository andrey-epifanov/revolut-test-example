package revolut.global;

import common.selenium.baseanalyser.BasePageAnalyser;
import common.selenium.baseanalyser.CommandCheckIsEnabled;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import revolut.data.Card;
import revolut.data.CurrencyVolume;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

/**
 * Created by aepifanov on 08.08.2017.
 */
public class RevolutApp extends AbstractApp {
    private Logger logger = LoggerFactory.getLogger(RevolutApp.class);

    private Card previous;

    @Step("Allow to continue")
    public void allowContinue(){
        logger.info("========== Precondition - Continue ====");
        for ( int i = 0; i < 6; i++ ) {
            try{
                driver.makeScreenShot("_" + i);
                driver.getInstance().findElement(By.className("android.widget.Button")).click();
                logger.info("Continue. Click.");
            } catch(Exception e) {
                logger.info("Continue not find");
            }
        }
    }

    @Step("Login")
    public void loginWithPhone(String phonePart){
        logger.info("========== Start Test ================");
        startPhoneAction.executeWith(phonePart);

        logger.info("========== Password ==================");
        passwordAction.execute();

        try{
            logger.info("========== Allow contact access ===========");
            if ( BasePageAnalyser.check(new CommandCheckIsEnabled(), allowContactAccessAction) )
                logger.info("Current page - allowContactAccess");
            allowContactAccessAction.execute();
        } catch(Exception e) {
            logger.info("Allow access - not need");
        }
    }

    @Step("Go to Top-up by card")
    public void goToTopUpByCard(){
        // иногда можем сюда не переходить
        logger.info("========== Top-Up ==========================");
        topAction.execute();

        logger.info("========== Add money via ===================");
        gotToMoneyViaCard();
    }

    @Step("Card delete")
    public void cardDelete(){
        logger.info("========== Card Delete ===================");
        BasePageAnalyser.check(new CommandCheckIsEnabled(), topUpByCardAction);
        previous = topUpByCardAction.getCurrentCard();

        driver.makeScreenShot();
        topUpByCardAction.cardDelete();
    }

    @Step("Card delete - ALL")
    public void cardDeleteAll(){
        while (topUpByCardAction.text_cardNumber.getText().length() > 0) {
            cardDelete();
        }
    }

    @Step("Card add")
    public void cardAdd(){
        logger.info("========== Card Add ===================");
        BasePageAnalyser.check(new CommandCheckIsEnabled(), topUpByCardAction);
        previous = topUpByCardAction.getCurrentCard();

        driver.makeScreenShot();
        if (topUpByCardAction.text_cardNumber.getText().length() > 0)
            topUpByCardAction.moveToAddNewCard();
        if (topUpByCardAction.text_cardNumber.getText().length() > 0)
            topUpByCardAction.moveToAddNewCard();

        Card card = Card.searchByLast4digit("7720");
        topUpByCardAction.cardAdd(card);
    }

    @Step("Go to Money Via Card")
    private void gotToMoneyViaCard() {
        List<WebElement> cardWrappers = driver.getInstance().findElements(By.id("com.revolut.revolut.test:id/item_title"));
        for(WebElement card:cardWrappers) {
            if("Card".equals(card.getText())) {
                card.click();
                return;
            }
        }
    }

    public Integer getCurrentMoney(String currencyType) throws Exception {
        Integer curent;
        driver.getWaiter().until(ExpectedConditions.visibilityOf(topAction.btn_topUp));
        if (BasePageAnalyser.check( new CommandCheckIsEnabled(), topAction)) {

            // todo: refactoring - go to  the corresponding currency type

            //TODO: text_views some time do not work - 3 number list is not work
//            try {
//                String moneyStr = topAction.getCurrancyValueInt().getText();
//                logger.info("Current money text = " + moneyStr);
//                curent = Integer.parseInt(moneyStr);  // some times - java.lang.NumberFormatException: For input string: "."
//            }catch (NumberFormatException nfe){
//                String moneyStr = topAction.text_views.get(3).getText();
//                logger.info("Current money text = " + moneyStr);
//                curent = Integer.parseInt(moneyStr);
//            }
            String moneyStr = topAction.text_amount_int.findElement(By.className("android.widget.TextView")).getText();
            curent = Integer.parseInt(moneyStr);
        } else {
            throw new Exception("this is not page - Top Page");
        }
        return curent;
    }

    @Step("Top-Up Money - by Card")
    public void topUpMoneyByCard(CurrencyVolume currencyVolume) {
        logger.info("========== Top-up by card ===================");
        topUpByCardAction.executeWith(currencyVolume, null); // , passwordAction.getLocationDigit1());
        driver.getWaiter().until(ExpectedConditions.visibilityOf(topAction.btn_topUp)); // wait loading process is finish
    }

    public boolean comparePreviousCardAndCurrent() {
    return topUpByCardAction.verifyCard(previous);
    }

    public String getCurrentCardNumber(){
        return topUpByCardAction.text_cardNumber.getText();
    }

//    private Point getCenter(WebElement element) {
//        Point upperLeft = element.getLocation();
//        Dimension dimensions = element.getSize();
//        return new Point(upperLeft.getX() + dimensions.getWidth()/2, upperLeft.getY() + dimensions.getHeight()/2);
//    }
//
//    public MobileElement scrollTo(String text){
//        return (MobileElement) driver.findElement(MobileBy.
//                AndroidUIAutomator("new UiScrollable(new UiSelector()"
//                        + ".scrollable(true)).scrollIntoView(resourceId(\"android:id/list\")).scrollIntoView("
//                        + "new UiSelector().text(\""+text+"\"))"));
//    }

}
