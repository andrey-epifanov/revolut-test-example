package revolut.example.unittest;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import revolut.data.Card;

/**
 * Created by aepifanov on 08.08.2017.
 */
public class CardTest {
    private Logger logger = LoggerFactory.getLogger(CardTest.class);

    @Test
    public void test1(){
        logger.info("4415 - " + Card.searchByLast4digit("4415"));
    }

    @Test
    public void test2(){
        logger.info("4415 extract - " + Card.extractLast4digits("5191330000004415"));
    }
}
