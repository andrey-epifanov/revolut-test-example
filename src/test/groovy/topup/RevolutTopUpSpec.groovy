package topup

import revolut.data.CurrencyVolume
import revolut.global.RevolutApp

/*
 * Copyright 2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RevolutTopUpSpec extends spock.lang.Specification {
    def revolutTest = new RevolutApp();
    def money;

    def setup() {
        revolutTest.setUp()
    }

    def "top up minimal money"() {
        when:
        revolutTest.allowContinue()
        revolutTest.loginWithPhone("662266");
        money = revolutTest.getCurrentMoney("GBP") // some times not work
        revolutTest.goToTopUpByCard()

        then:
        revolutTest.topUpMoneyByCard(new CurrencyVolume("GBP", "10"))

        expect:
        money + 10 == revolutTest.getCurrentMoney("GBP")
        //revolutTest.getCurrentMoney("GBP") > 10 // second variant

        cleanup:
        revolutTest.tearDown()
    }

}  