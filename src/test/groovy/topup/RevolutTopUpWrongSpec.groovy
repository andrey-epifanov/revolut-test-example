package topup

import common.generalhelpers.RandomDataGenerator
import revolut.data.CurrencyVolume
import revolut.global.RevolutApp

/*
 * Copyright 2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RevolutTopUpWrongSpec extends spock.lang.Specification {
    def revolutTest = new RevolutApp();

    def setup() {
        revolutTest.setUp()
    }

    def "top up minimal money"() {
        when:
        revolutTest.allowContinue()
        revolutTest.loginWithPhone("662266");

        then:
        revolutTest.goToTopUpByCard()

        expect:
        revolutTest.topUpMoneyByCard(new CurrencyVolume("GBP", "9"))
        revolutTest.topUpMoneyByCard(new CurrencyVolume("GBP", RandomDataGenerator.getRandomSpecialChar()))
        revolutTest.topUpMoneyByCard(new CurrencyVolume("GBP", RandomDataGenerator.getRandomString(2)))

        cleanup:
        revolutTest.tearDown()
    }

}  